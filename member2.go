package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"time"

	//"github.com/go-chi/chi/v5"
	// "github.com/containerd/console"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Member struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name        string             `json:"name" bson:"name"`
	NationalID  string             `json:"nat_id" bson:"nat_id"`
	PhoneNumber string             `json:"phone_number" bson:"phone_number"`

	Savings []Savings `json:"savings" bson:"savings"`
	Loans   []Loans   `json:"loans" bson:"loans"`

	CreatedAt string `json:"created_at" bson:"created_at"`
}

func handleCreateMember(w http.ResponseWriter, r *http.Request) {
	// Parse the JSON request body
	var member Member
	err := json.NewDecoder(r.Body).Decode(&member)
	if err != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}

	// Set the CreatedAt field with the current time
	member.CreatedAt = time.Now().Format(time.RFC3339)

	collection := client.Database(dbName).Collection(members_collection)
	_, err = collection.InsertOne(context.Background(), member)
	//fmt.Print(member)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		fmt.Println("error", err)
		return
	}

	// Simulate storing the data to a database or perform any other necessary operations
	// In a real-world scenario, you would persist the data to your database

	// Print the received data for demonstration purposes
	fmt.Printf("Received Member: %+v\n", member)

	// Respond with a success message
	//response := map[string]string{"status": "success"}
	responseJSON, err := json.Marshal(member)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Set the Content-Type header to application/json
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response
	w.Write(responseJSON)
}

// func handleGetMemberByName(w http.ResponseWriter, r *http.Request) {

// 	params := mux.Vars(r)
// 	id := params["id"]

// 	fmt.Println("ID passed", id)

// 	fmt.Print("get nurse by name has been called....", id)
// 	// Create a case-insensitive regular expression for the nurse name
// 	//regex := primitive.Regex{Pattern: id, Options: "i"}

// 	// Retrieve the specific nurse from MongoDB using a case-insensitive query
// 	collection := client.Database(dbName).Collection(members_collection)
// 	var member Member
// 	err := collection.FindOne(context.Background(), bson.M{"_id": id}).Decode(&member)
// 	if err != nil {
// 		response := map[string]string{"error": "nurse not found"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Respond with the retrieved nurse in JSON format
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(member)
// }

func handleGetMemberByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	fmt.Println("ID passed", id)

	fmt.Print("get member by ID has been called....", id)

	// Retrieve the specific member from MongoDB using the provided ID
	collection := client.Database(dbName).Collection(members_collection)
	var member Member
	err := collection.FindOne(context.Background(), bson.M{"name": id}).Decode(&member)
	if err != nil {

		fmt.Println("err:", err)
		response := map[string]string{"error": "member not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved member in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(member)
}

func handleAddSavingsById(w http.ResponseWriter, r *http.Request) {
	//id := chi.URLParam(r, "id")
	fmt.Println("add savings by id has been")

	params := mux.Vars(r)
	id := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		http.Error(w, "Invalid nurse ID", http.StatusBadRequest)
		return
	}

	fmt.Println("recieved id:->", id)

	decoder := json.NewDecoder(r.Body)
	var incomingSavings Savings
	err = decoder.Decode(&incomingSavings)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	//currentTime := time.Now()

	incomingSavings.CreatedAt = time.Now().Format(time.RFC3339)

	collection := client.Database(dbName).Collection(members_collection)
	filter := bson.M{"_id": objID}
	//update := bson.M{"$push": incomingSavings}
	update := bson.M{"$push": bson.M{"savings": incomingSavings}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error Adding Savings", http.StatusInternalServerError)
		return
	}


	http.Error(w, "Success", http.StatusInternalServerError)

}

func handleAddLoansById(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id := params["id"]

	fmt.Println("passed loan id", id)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		http.Error(w, "Invalid nurse ID", http.StatusBadRequest)
		return
	}

	fmt.Println("recieved id:->", id)

	decoder := json.NewDecoder(r.Body)
	var incomingLoans Loans
	err = decoder.Decode(&incomingLoans)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	incomingLoans.CreatedAt = time.Now().Format(time.RFC3339)

	collection := client.Database(dbName).Collection(members_collection)
	filter := bson.M{"_id": objID}
	//update := bson.M{"$push": incomingSavings}
	update := bson.M{"$push": bson.M{"loans": incomingLoans}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating nurse", http.StatusInternalServerError)
		return
	}

	http.Error(w, "Success", http.StatusInternalServerError)
	//return

	// if err != nil {

	// 	http.Error(w, "Success", http.StatusInternalServerError)
	// 	return

	// }

}

func handleGetMembers(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(members_collection)

	// Define a slice to store retrieved facilities
	var members []Member

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving members", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var member Member
		if err := cursor.Decode(&member); err != nil {
			http.Error(w, "Error decoding member", http.StatusInternalServerError)
			return
		}
		members = append(members, member)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(members)
}

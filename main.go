package main

import (
	//"context"
	//"encoding/json"
	"context"
	"fmt"

	//"log"
	"net/http"

	//"github.com/gorilla/handlers"
	//"github.com/gorilla/mux"
	// "gorm.io/driver/sqlite"
	// "gorm.io/gorm"

	// "github.com/go-chi/chi/v5"
	// "github.com/go-chi/chi/v5/middleware"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	//"go.mongodb.org/mongo-driver/mongo/options"
	//"gopkg.in/mail.v2"
	//"gorm.io/datatypes"
	// "gorm.io/driver/sqlite"
	// "gorm.io/gorm"
	// "log"
)

// type UserWithJSON struct {
// 	gorm.Model
// 	Name       string
// 	Attributes datatypes.JSON
// }

// package level variable
var dbName = "chama-db"

//var db *gorm.DB

var nures_collection = "new_nurses_collection"

var nurse_logs_collection = "nurse_logs_collection"

// var settings_collection = "settings_collection"
var settings_collection = "new_settings_collection"

var members_collection = "members_collection"

var secretKey = []byte("secret-key")
var users_collection = "users_collection"

// var FacilityCollection = "Fac"
// var DriverCollection = "Drivers"
// var VehicleCollection = "Vehicles"
// var BrokerCollection = "Brokers"

var mongoURI = "mongodb+srv://dandisolutions09:GAFmLYi25DojAuM1@tms-cluster.cp6qfmq.mongodb.net/"

var client *mongo.Client

// type UserWithJSON struct {
// 	gorm.Model
// 	Name       string         `json:"name"`
// 	Attributes datatypes.JSON `json:"attributes"`
// }

// type NurseData struct {
// 	gorm.Model
// 	Name               string         `json:"name"`
// 	Department         string         `json:"department"`
// 	Wards              datatypes.JSON `json:"wards"`
// 	Grade              string         `json:"grade"`
// 	SignatureImageData string         `json:"signatureImageData"`
// }

// CustomHandler is a custom handler function.
func CustomHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("This is a new custom handler!"))
}

// func HandlePostRequest(w http.ResponseWriter, r *http.Request) {
// 	// Parse the JSON request body
// 	var member Member
// 	err := json.NewDecoder(r.Body).Decode(&member)
// 	if err != nil {
// 		http.Error(w, "Invalid JSON", http.StatusBadRequest)
// 		return
// 	}

// 	// Set the CreatedAt field with the current time
// 	member.CreatedAt = time.Now().Format(time.RFC3339)

// 	// Simulate storing the data to a database or perform any other necessary operations
// 	// In a real-world scenario, you would persist the data to your database

// 	// Print the received data for demonstration purposes
// 	fmt.Printf("Received Member: %+v\n", member)

// 	// Respond with a success message
// 	//response := map[string]string{"status": "success"}
// 	responseJSON, err := json.Marshal(member)
// 	if err != nil {
// 		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
// 		return
// 	}

// 	// Set the Content-Type header to application/json
// 	w.Header().Set("Content-Type", "application/json")

// 	// Write the JSON response
// 	w.Write(responseJSON)
// }

// func SendMail() {
// 	// Sender's email credentials
// 	from := "collinsdon09@gmail.com"
// 	password := "zuhn wzwz vkef eglw"

// 	// Recipient email address
// 	to := "dandisolutions09@gmail.com"

// 	// SMTP server configuration
// 	smtpHost := "smtp.gmail.com"
// 	smtpPort := 587

// 	// Message content
// 	subject := "Test Subject"
// 	body := "This is a test email body."

// 	// Create a new email message
// 	m := mail.NewMessage()
// 	m.SetHeader("From", from)
// 	m.SetHeader("To", to)
// 	m.SetHeader("Subject", subject)
// 	m.SetBody("text/plain", body)

// 	// Set up the SMTP client
// 	d := mail.NewDialer(smtpHost, smtpPort, from, password)

// 	// Send the email
// 	err := d.DialAndSend(m)
// 	if err != nil {
// 		fmt.Println("there was an error sending email")
// 		panic(err)

// 	}

// 	fmt.Println("Email sent successfully.")
// }

func init() {

	var err error

	// db, err := gorm.Open(sqlite.Open("mydatabase.db"), &gorm.Config{})
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// //defer db.Close()

	// db.AutoMigrate(&UserS{})

	// Set up MongoDB client
	clientOptions := options.Client().ApplyURI(mongoURI)

	client, err = mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		fmt.Println("Error pinging MongoDB:", err)
		return
	}

	fmt.Println("Connected to MongoDB")
}

// func initDB() {
// 	var err error
// 	db, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	db.AutoMigrate(&UserWithJSON{})
// 	db.AutoMigrate(&NurseData{})
// }

// func createNurse(w http.ResponseWriter, r *http.Request) {
// 	var newUser NurseData
// 	err := json.NewDecoder(r.Body).Decode(&newUser)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	result := db.Create(&newUser)
// 	if result.Error != nil {
// 		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	w.WriteHeader(http.StatusCreated)
// 	json.NewEncoder(w).Encode(newUser)
// }

// // GetAllNurseData retrieves all nurse data from the database
// func GetAllNurseDataHandler(w http.ResponseWriter, r *http.Request) {
// 	var nurses []NurseData
// 	result := db.Find(&nurses)
// 	if result.Error != nil {
// 		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(nurses)
// }

// // UpdateNurseDataHandler handles the PUT request to update a specific nurse by ID
// func UpdateNurseDataHandler(w http.ResponseWriter, r *http.Request) {
// 	params := mux.Vars(r)
// 	var nurse NurseData
// 	result := db.First(&nurse, params["id"])
// 	if result.Error != nil {
// 		http.Error(w, result.Error.Error(), http.StatusNotFound)
// 		return
// 	}

// 	var updatedNurse NurseData
// 	err := json.NewDecoder(r.Body).Decode(&updatedNurse)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	result = db.Model(&nurse).Updates(updatedNurse)
// 	if result.Error != nil {
// 		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(nurse)
// }

// // DeleteNurseDataHandler handles the DELETE request to delete a specific nurse by ID
// func DeleteNurseDataHandler(w http.ResponseWriter, r *http.Request) {
// 	params := mux.Vars(r)
// 	var nurse NurseData
// 	result := db.Delete(&nurse, params["id"])
// 	if result.Error != nil {
// 		fmt.Println("delete unsuccessfully!")
// 		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	w.WriteHeader(http.StatusNoContent)
// }

// Define a struct for your data (adjust fields as needed)
func main() {
	// Create a new Gorilla Mux router

	// var err error
	// db, err = gorm.Open(sqlite.Open("mydatabase.db"), &gorm.Config{})
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// // AutoMigrate will create the table based on the User struct
	// db.AutoMigrate(&UserS{})

	//SendMail()

	//r := chi.NewRouter()

	//r.Use(middleware.Logger)

	//initDB()

	router := mux.NewRouter()

	//Use the handlers.CORS middleware to handle CORS
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	// r.Use(cors.Handler(cors.Options{
	// 	// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
	// 	AllowedOrigins: []string{"https://*", "http://*"},
	// 	// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
	// 	AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	// 	AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
	// 	ExposedHeaders:   []string{"Link"},
	// 	AllowCredentials: false,
	// 	MaxAge:           300, // Maximum value not ignored by any of major browsers
	// }))

	//r.Get("/custom", CustomHandler)
	//r.Post("/add-member", handleAddMember)
	//	r.Post("/add-member", handleCreateMember)

	//r.Post("/add-nurse", handleAddNurse)

	// r.Get("/member/{name}", handleGetMemberByName)
	// r.Get("/get-members", handleGetMembers)
	// r.Put("/add-savings/{id}", handleAddSavingsById)
	// r.Put("/add-loans/{id}", handleAddLoansById)

	// // Attach the CORS middleware to your router

	// //FACILITY ROUTES
	router.HandleFunc("/add-member", handleCreateMember).Methods("POST")
	router.HandleFunc("/get-members", handleGetMembers).Methods("GET")
	//router.HandleFunc("/member/{name}", handleGetMemberByName).Methods("GET")
	router.HandleFunc("/member/{id}", handleGetMemberByID).Methods("GET")

	router.HandleFunc("/add-savings/{id}", handleAddSavingsById).Methods("PUT")
	router.HandleFunc("/add-loans/{id}", handleAddLoansById).Methods("PUT")

	// //GET BY ID
	// //LOGIN ROUTES
	// router.HandleFunc("/login", LoginHandler).Methods("POST")
	// // router.HandleFunc("/protected", ProtectedHandler).Methods("GET")
	// router.HandleFunc("/get-username/{username}", handleGetUserByName).Methods("GET")
	// router.HandleFunc("/register", RegisterUser).Methods("POST")
	// router.HandleFunc("/get-users", handleGetUsers).Methods("GET")
	// router.HandleFunc("/update-user/{id}", handleUpdateUser).Methods("PUT")
	// router.HandleFunc("/reset-password/{id}", handleResetPassword).Methods("PUT")

	// router.HandleFunc("/encrypt", encryption).Methods("GET")
	// router.HandleFunc("/create-new-settings", handlecreateSettings).Methods("POST")
	// router.HandleFunc("/update-settings/{id}", handleUpdateSetting).Methods("PUT")
	// router.HandleFunc("/get-settings", handleGetSettings).Methods("GET")

	// // http://localhost:8080/update-user/

	// //LOGS ENDPOINTS
	// router.HandleFunc("/create-log", handlecreateNurseLog).Methods("POST")
	// router.HandleFunc("/get-logs", handleGetNurseLogs).Methods("GET")

	// //SQLITE generation
	// //	router.HandleFunc("/users", createUser).Methods("POST")
	// router.HandleFunc("/create", createNurse).Methods("POST")
	// router.HandleFunc("/nurses", GetAllNurseDataHandler).Methods("GET")
	// router.HandleFunc("/update-nurse/{id}", UpdateNurseDataHandler).Methods("PUT")
	// router.HandleFunc("/remove-nurse/{id}", DeleteNurseDataHandler).Methods("DELETE")

	// Start the server on port 8080
	http.Handle("/", corsHandler(router))
	fmt.Println("Server listening on :8080")
	err := http.ListenAndServe(":8080", nil)
	fmt.Println("error", err)
}

package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"

	// "io"

	// "io/ioutil"
	"net/http"
	// "os"
	"time"

	"github.com/golang-jwt/jwt/v5" // "github.com/gorilla/mux"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	// "golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Username  string             `json:"username" bson:"username"`
	Password  string             `json:"password" bson:"password"`
	Salt      []byte             `json:"salt" bson:"salt"`
	Role      string             `json:"role" bson:"role"`
	Status    string             `json:"status" bson:"status"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
}

type TimeStamp struct {
	DateTime string `json:"date_time"`
	UTC      int    `json:"utc_time"`
}

func getCurrentTime() (string, error) {
	url := "http://worldtimeapi.org/api/timezone/Europe/London"
	response, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("error making GET request: %w", err)
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("error reading response body: %w", err)
	}

	return string(body), nil
}

func randomSecret(length uint32) ([]byte, error) {
	secret := make([]byte, length)

	_, err := rand.Read(secret)
	if err != nil {
		return nil, err
	}

	return secret, nil
}

func handleGetUserByName(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get driver by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	username := params["username"]

	// Convert the Driver ID to a MongoDB ObjectID

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection(users_collection)
	var usr User
	err := collection.FindOne(context.Background(), bson.M{"username": username}).Decode(&usr)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")

	//passwordVerification()

	json.NewEncoder(w).Encode(usr)
}

func createToken(username string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"username": username,
			"exp":      time.Now().Add(time.Hour * 24).Unix(),
		})

	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func handleGetUsers(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(users_collection)

	// Define a slice to store retrieved facilities
	var users []User

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var user User
		if err := cursor.Decode(&user); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		users = append(users, user)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Decode the incoming JSON request into a User struct
	var u User
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Hash the incoming password using Argon2id
	//argon2IDHash := NewArgon2idHash(1, 32, 64*1024, 32, 256)
	// hashSaltIncomingPass, err := argon2IDHash.GenerateHash(u.Password, nil)
	// if err != nil {
	// 	http.Error(w, "Error hashing password", http.StatusInternalServerError)
	// 	return
	// }

	// Make a GET request to retrieve the stored hash from another endpoint
	url := "http://localhost:8080/get-username/" + u.Username
	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	// Read the response body
	storedHash, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	fmt.Println("stored hash", storedHash)

	// Unmarshal the stored hash from JSON
	var storedUser User
	if err := json.Unmarshal(storedHash, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	passwordsMatch := comparePasswords(u.Password, storedUser.Password, storedUser.Salt)
	fmt.Println("Passwords Match:", passwordsMatch)

	if passwordsMatch {
		fmt.Println("Authentication successful")

		fmt.Println("storedRole", storedUser.Role)

		createToken(u.Username)

		// json.NewEncoder(w).Encode(map[string]string{
		// 	"message": "Authentication successful",
		// 	// "token":   tokenString,
		// })

		w.Write([]byte("Success," + storedUser.Role))
		// w.Write([]byte())

	} else {

		w.Write([]byte("AuthFailed"))
		// http.Error(w, "Login failed", http.StatusUnauthorized)

	}

}

func RegisterUser(w http.ResponseWriter, r *http.Request) {

	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var inc_data User
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming data", inc_data)

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	// fmt.Println("Password:", inc_data.Password)
	// fmt.Println("Hashed Password:", hashedPassword)
	// fmt.Printf("Salt: %x\n", salt)

	// Unmarshal JSON string into a struct (using a pointer to the variable)

	currentTime := time.Now()

	if currentTime.Location().String() != "Europe/London" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Europe/London")
		if err != nil {
			fmt.Println("Error loading London location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		inc_data.CreatedAt = timeString

		inc_data.Password = hashedPassword
		inc_data.Salt = salt

		// fmt.Println("to be saved in db:->", inc_data)

		collection := client.Database(dbName).Collection(users_collection)
		_, err = collection.InsertOne(context.Background(), inc_data)
		fmt.Print(inc_data)
		if err != nil {
			http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		// fmt.Fprintf(w, "Nurse inserted Successfully")

		response := map[string]string{"message": "User inserted successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {
		fmt.Println("Current Time is already in Europe/London time zone.")
	}

	//timeString := currentTime.Format("2006-01-02 15:04:05")
	//fmt.Println("Current time as string:", timeString)

}

func ProtectedHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	tokenString := r.Header.Get("Authorization")
	if tokenString == "" {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Missing authorization header")
		return
	}
	tokenString = tokenString[len("Bearer "):]

	err := verifyToken(tokenString)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Invalid token")
		return
	}

	fmt.Fprint(w, "Welcome to the the protected area")

}

func verifyToken(tokenString string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})

	if err != nil {
		return err
	}

	if !token.Valid {
		return fmt.Errorf("invalid token")
	}

	return nil
}

// func handleUpdateUser(w http.ResponseWriter, r *http.Request) {
// 	// Get the facility ID from the request parameters
// 	params := mux.Vars(r)
// 	userID := params["id"]

// 	// Convert the facility ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(userID)
// 	if err != nil {
// 		http.Error(w, "Invalid User", http.StatusBadRequest)
// 		return
// 	}

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var updatedData User
// 	err = decoder.Decode(&updatedData)
// 	if err != nil {
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return

// 	}

// 	fmt.Println("incoming data:", updatedData.Password)

// 	//argon2IDHash := NewArgon2idHash(1, 32, 64*1024, 32, 256)

// 	// Update the specific facility in MongoDB
// 	collection := client.Database(dbName).Collection(users_collection)
// 	filter := bson.M{"_id": objID}
// 	update := bson.M{"$set": updatedData}

// 	_, err = collection.UpdateOne(context.Background(), filter, update)
// 	if err != nil {
// 		http.Error(w, "Error updating user", http.StatusInternalServerError)
// 		return
// 	}

// 	// Respond with a success message
// 	//fmt.Fprintf(w, "Facility updated successfully")

// 	response := map[string]string{"message": "User updated successfully"}
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(response)
// }

func handleUpdateUser(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData User
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Exclude password and salt from the update
	updateData := bson.M{}
	if updatedData.Username != "" {
		updateData["username"] = updatedData.Username
	}
	if updatedData.Status != "" {
		updateData["status"] = updatedData.Status
	}

	if updatedData.Role != "" {
		updateData["role"] = updatedData.Role
	}
	// Add other fields that you want to update...

	// Update the specific user in MongoDB
	collection := client.Database(dbName).Collection(users_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updateData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating user", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "User updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleResetPassword(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData User
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("INCOMING DATA:->", updatedData)

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(updatedData.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	fmt.Println("Password:", updatedData.Password)
	fmt.Println("Hashed Password:", hashedPassword)
	fmt.Printf("Salt: %x\n", salt)

	currentTime := time.Now()
	//timeString := currentTime.Format("2006-01-02 15:04:05")
	timeString := currentTime.Format("Monday, 02 January 2006 15:04:05")
	fmt.Println("Current time as string:", timeString)

	updatedData.CreatedAt = timeString

	updatedData.Password = hashedPassword
	updatedData.Salt = salt

	// Exclude password and salt from the update
	updateData := bson.M{}
	if updatedData.Username != "" {
		updateData["password"] = updatedData.Password
	}
	if updatedData.Status != "" {
		updateData["salt"] = updatedData.Salt
	}

	// Add other fields that you want to update...

	// Update the specific user in MongoDB
	collection := client.Database(dbName).Collection(users_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updateData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating user", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "User updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Settings_struct struct {
	ID                primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	PositionOptions   []string           `json:"position_options" bson:"position_options"`
	WardOptions       []string           `json:"ward_options" bson:"ward_options"`
	DepartmentOptions []string           `json:"department_options" bson:"department_options"`
	CreatedAt         string             `json:"created_at" bson:"created_at"`
}

func handlecreateSettings(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	//fmt.Printf(decoder)
	var data Settings_struct
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update createdAt with the current time
	//data.CreatedAt, err = time.Parse(time.RFC3339, time.Now().Format("2006-01-02 15:04:05"))

	fmt.Print("created at:", data.CreatedAt)

	currentTime := time.Now()
	//timeString := currentTime.Format("2006-01-02 15:04:05")
	timeString := currentTime.Format("Monday, 02 January 2006 15:04:05")
	fmt.Println("Current time as string:", timeString)

	data.CreatedAt = timeString

	fmt.Println("postion options:-->", data.PositionOptions)
	fmt.Println("ward options:-->", data.WardOptions)
	// fmt.Println("incoming log:-->", data)

	//utcTime, err := time.Parse(time.RFC3339, timestamp)

	// if err != nil {
	// 	fmt.Print("Failed to convert time")

	// }

	// utcTime, err := time.Parse(time.RFC3339, utcTimeString)

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(settings_collection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	response := map[string]string{"message": "Settings inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleUpdateSetting(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid settings ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Settings_struct
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated data", updatedData)

	// Update the specific facility in MongoDB
	collection := client.Database(dbName).Collection(settings_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updatedData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating nurse", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Facility updated successfully")

	response := map[string]string{"message": "Facility updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetSettings(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(settings_collection)

	// Define a slice to store retrieved facilities
	var settings []Settings_struct

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var setting Settings_struct
		if err := cursor.Decode(&setting); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		settings = append(settings, setting)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(settings)
}

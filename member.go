package main

import (
	// "container/list"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	// "time"

	// "time"

	//"github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	// "go.mongodb.org/mongo-driver/bson"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "gorm.io/gorm"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)



type Savings struct {
	AmountSaved string `json:"amount_saved" bson:"amount_saved"`
	CreatedAt   string `json:"created_at" bson:"created_at"`
}

type Loans struct {
	AmountBorrowed string `json:"amount_borrowed" bson:"amount_borrowed"`
	CreatedAt      string `json:"created_at" bson:"created_at"`
}

type Member_logs struct {
	// gorm.Model
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Username  string             `json:"username" bson:"username"`
	UserRole  string             `json:"user_role" bson:"user_role"`
	Activity  string             `json:"activity" bson:"activity"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
}



func handlecreateMemberLog(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	//fmt.Printf(decoder)
	var data Nurse_logs
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update createdAt with the current time
	//data.CreatedAt, err = time.Parse(time.RFC3339, time.Now().Format("2006-01-02 15:04:05"))

	fmt.Print("created at:", data.CreatedAt)

	currentTime := time.Now()
	//timeString := currentTime.Format("2006-01-02 15:04:05")
	timeString := currentTime.Format("Monday, 02 January 2006 15:04:05")
	fmt.Println("Current time as string:", timeString)

	data.CreatedAt = timeString

	fmt.Println("username:-->", data.Username)
	fmt.Println("user role:-->", data.UserRole)
	// fmt.Println("incoming log:-->", data)

	//utcTime, err := time.Parse(time.RFC3339, timestamp)

	// if err != nil {
	// 	fmt.Print("Failed to convert time")

	// }

	// utcTime, err := time.Parse(time.RFC3339, utcTimeString)

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(nurse_logs_collection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	response := map[string]string{"message": "Nurse logs inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetMemberLogs(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(nurse_logs_collection)

	// Define a slice to store retrieved facilities
	var nurses []Nurse_logs

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var nurse Nurse_logs
		if err := cursor.Decode(&nurse); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		nurses = append(nurses, nurse)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(nurses)
}

func handleAddMember(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var data Nurse
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("data", data)

	currentTime := time.Now()

	if currentTime.Location().String() != "Europe/London" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Europe/London")
		if err != nil {
			fmt.Println("Error loading London location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		data.CreatedAt = timeString

		//data.CreatedAt = "2023-01-02 15:04:05" //EXPIRY TEST
		//data.CreatedAt = "2023-11-22 16:04:05" //AMBER TEST

		//		November 22, 2023.

		// fmt.Println("to be saved in db:->", inc_data)

		collection := client.Database(dbName).Collection(nures_collection)
		_, err = collection.InsertOne(context.Background(), data)
		fmt.Print(data)
		if err != nil {
			http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		// fmt.Fprintf(w, "Nurse inserted Successfully")

		response := map[string]string{"message": "Nurse inserted successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {
		fmt.Println("Current Time is already in Europe/London time zone.")
	}
	// //timeString := currentTime.Format("2006-01-02 15:04:05")
	// timeString := currentTime.Format("Monday, 02 January 2006 15:04:05")
	// fmt.Println("Current time as string:", timeString)

	// data.CreatedAt = timeString

	// // Insert data into MongoDB
	// collection := client.Database(dbName).Collection(nures_collection)
	// _, err = collection.InsertOne(context.Background(), data)
	// fmt.Print(data)
	// if err != nil {
	// 	http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
	// 	return
	// }

	// // Respond with a success message
	// // fmt.Fprintf(w, "Nurse inserted Successfully")

}

//GET FACILITY BY ID



//GET ALL FACILITIES

// func handleGetMembers(w http.ResponseWriter, r *http.Request) {
// 	collection := client.Database(dbName).Collection(nures_collection)

// 	// Define a slice to store retrieved facilities
// 	var nurses []Nurse

// 	// Retrieve all documents from the collection
// 	cursor, err := collection.Find(context.Background(), bson.D{})
// 	if err != nil {
// 		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
// 		return
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and decode documents into the facilities slice
// 	for cursor.Next(context.Background()) {
// 		var driver Nurse
// 		if err := cursor.Decode(&driver); err != nil {
// 			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
// 			return
// 		}
// 		nurses = append(nurses, driver)
// 	}

// 	// Respond with the retrieved facilities in JSON format
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(nurses)
// }

//GET MEMBER BY ID

//
// //UPDATE MEMBER

func handleUpdateMember(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid nurse ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Nurse
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Europe/London" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Europe/London")
		if err != nil {
			fmt.Println("Error loading London location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific facility in MongoDB
		collection := client.Database(dbName).Collection(nures_collection)
		filter := bson.M{"_id": objID}
		update := bson.M{"$set": updatedData}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Facility updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

// // DELETE FACILITY
func handleDeleteMember(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Delete the specific facility in MongoDB
	collection := client.Database(dbName).Collection(nures_collection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Facility deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
